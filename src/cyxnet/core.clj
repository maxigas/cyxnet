(ns cyxnet.core
  (:gen-class)
  (:use lacij.edit.graph
        lacij.view.graphview
        lacij.layouts.layout))

(def styles
  {:title {:rx 0 :ry 0 :fill "khaki"}
   :person {:rx 3 :ry 3 :fill "yellowgreen"}
   :country {:rx 0 :ry 0 :fill "powderblue"}
   :employer {:rx 0 :ry 0 :fill "burlywood"}
   :sentence {:rx 0 :ry 0 :fill "darkorange"}
   :concept {:rx 6 :ry 0 :fill "white"}})

(def prx 0)

(def p:ont
  {:rx 0 :tx 0})

(def default
  {:font-family "Verdana, Arial, sans-serif" :font-size "10px" :font-weight "bold"})

(def repl clojure.string/replace)

(defn str->kw
  "Make a string a keyword"
  [s]
  (keyword (repl s " " "_")))

(defn +node
  [graph kind label]
  (let [height (if (sequential? label) (* (count label) 14) 14)
        columns (if (sequential? label) (reduce max (map count label)) (count label))
        width (+ (* columns 7) 10)
        kword (str->kw (reduce str label))]
    (println "Add: " (str kind) label)
    (add-node graph kword "" :width width :height height :rx (-> styles kind :rx)
              :style {:fill (-> styles kind :fill)})))

(defn +label
  [graph kind label]
  (let [kword (str->kw (reduce str label))]
    (add-label graph kword label :style {:font-family "Verdana, Arial, sans-serif" :font-size "10px" :font-weight "bold"})))

(defn +o
  [graph kind label]
  (+label (+node graph kind label) kind label))

(defn +->
  [graph from to label]
  (println "+->" from to label)
  (let [from (str->kw (reduce str from))
        to   (str->kw (reduce str to))
        kword (str->kw (str (str from) "->" (str to)))]
    (println "+->" kword)
    (-> graph
        (add-edge kword from to)
        (add-label kword label :style default))))

(defn wrote
  "Person wrote title."
  [graph person title year]
  (let [year (str year)]
    (-> graph
        (+o :person person)
        (+o :title [title year])
        (+-> person [title year] "wrote"))))

(defn invented
  "Person invented thing."
  [graph person thing]
  (-> graph
      (+o :person person)
      (+o :concept thing)
      (+-> person thing "invented")))

(defn worked-in
  "Person invented thing."
  [graph person country]
  (-> graph
      (+o :person person)
      (+o :country country)
      (+-> person country "worked in")))

(defn worked-at
  "Person worked at employer."
  [graph person employer]
  (-> graph
      (+o :person person)
      (+o :employer employer)
      (+-> person employer "worked at")))

(defn said
  "Person worked at employer."
  [graph person sentence]
  (let [sentence (str "“" sentence "”")]
  (-> graph
      (+o :person person)
      (+o :sentence sentence)
      (+-> person sentence "said"))))

(defn gen-graph
  []
  (let [nw "Norbert Wiener"
        gb "Gregory Bateson"]
  (-> (graph :width 1200 :height 600)
      (add-default-edge-style :stroke "lightgrey")
      (+o :concept "CYBERNETICS")
      (wrote "Gregory Bateson" "Steps to an Ecology of Mind" 1972)
      (wrote "Gregory Bateson" "Balinese Character: A Photographic Analysis" 0)
      (wrote "Gregory Bateson" "A Sacred Unity: Further Steps" 0)
      (wrote "Norbert Wiener" "Cybernetics: Control and Communication" 1948)
      (wrote "Norbert Wiener" "The Human Use of Human Beings" 1950)
      (wrote "Norbert Wiener" "Gold & Golem Inc." 1966)
      (wrote "Norbert Wiener" "“Yellow Peril”" 1966)
      (said "Norbert Wiener" "Information is informaxtion, not matter or energy.")
      (worked-in "Gregory Bateson" "USA")
      (worked-in "Norbert Wiener" "USA")
      (worked-in "Norbert Wiener" "Mexico")
      (worked-at "Norbert Wiener" "MIT")
      (worked-at "Norbert Wiener" ["Biological Computer Laboratory" "University of Illinois"])
      (invented "Gregory Bateson" "double bind")
      (invented nw "information")
      (invented nw "feedback")
      (+-> "Gregory Bateson" "CYBERNETICS" "founder of")
      (+-> "Norbert Wiener" "CYBERNETICS" "founder of"))))

(defn -main []
  (let [g (-> (gen-graph)
;              (layout :radial :flow :out :radius 500)
;              (layout :hierarchical :flow :out)
              (layout :naive); :radius 50)
              (build))]
    (export g "/tmp/cyx.svg" :indent "yes")))
